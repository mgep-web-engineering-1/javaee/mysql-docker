# MySQL Docker

We have 2 volumes:

1. For loading mvc_exercise.sql file to the database on creation.
1. To store changes we made in the future.

To make it work:

```bash
docker-compose up -d
```
